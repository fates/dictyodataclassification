\newcommand{\DD}{\mathcal{D}}
\newcommand{\Neighb}{\mathcal{N}}
\newcommand{\NN}{\texttt{N}}
\newcommand{\EE}{\texttt{E}}
\newcommand{\RR}{\texttt{R}}
\newcommand{\Am}[2]{\mathcal{A}^{#1}_{#2}}
\newcommand{\dini}{d_{\mathrm{ini}}}
\newcommand{\patt}{p_{\mathrm{att}}}
\newcommand{\Pag}{p_{\mathrm{ag}}}
\newcommand{\dstar}{\delta^*}
\newcommand{\Nset}{\widetilde{N}^{t}_{c}}
\newcommand{\Aset}{\widetilde{A}^{t}_{c}}
\newcommand{\Rset}{\widetilde{R}^{t}_{c}}
\newcommand{\Eset}{\widetilde{E}^{t}_{c}}
\newcommand{\dH}{d_\mathrm{H}}

\section*{Introduction}

Biology is an inexhaustible source of inspiration for computer science. Indeed, many phenomena we observe in living organisms can be understood in terms of simple interacting processes which create a self-organising process with a rich behaviour, e.g. self-reproduction~\cite{Book_Neumann}, cellular sorting~\cite{article_Deutsch}, etc. 
%
The model we present here aims at performing a clustering task. 
Various nature-inspired models have already been explored to perform this task~\cite{HanMey07,NanPan14}.
The particularity of our model is that it inspired from the behaviour of the social amoeba {\em Dictyostelium discoideum} and that it is a cellular automaton model, that is, space, time and states of components are discrete and the interactions are purely local.

Our proposition relies on a previous cellular automata model of this social amoeba~\cite{article_modele_1type}.
This original model was used to gather thousands of particles initially scattered randomly on a lattice without any central coordination.  From this original model, a variant was proposed to locate a hidden source on a lattice with the process called {\em infotaxis}, which is specific to the case where the detection of the source needs to be achieved with rare events~\cite{Fat16}.

Our goal now is to continue this work of exploration of the amoebae models and examine here whether they could be used to perform data clustering: imagine that we have set of objects initially random scattered on a lattice and we need to group this data into classes which share some similarity. The difficulty of the task lies in the propagation of information which needs to be achieved by simple cells with as few states as possible and which obey local rules, more specifically they should interact only with their nearest neighbours. 

We present our model with formal definitions in Sec.~\ref{sec:presentation}. This presentation is then followed by an empirical observation of its behaviour (Sec.~\ref{sec:qualExp}), the description of a quantification tool (Sec.~\ref{sec:criteria}) and a first sequence of statistical experiments (Sec.~\ref{sec:quantExp}). We then briefly explore some properties of robustness of the model~(Sec.~\ref{sec:robur}) and say a few words on the work that remains to be tackled.

%%%%%%%%%%%%%%%%%%
\section{Presentation of the model}
\label{sec:presentation}

Our model is described as a stochastic dynamical system where space, time and states are discrete. It is composed of two layers: the first layer corresponds to the environment, which propagates reaction-diffusion waves. The second layer contains the amoebae, which move and interact with the environment. Our objective is to design a system which will be as simple as possible in terms of cell states and behaviour, and for which the amoebae form groups according to the data they contain.

Space is modelled as a two-dimensional lattice with periodic boundary condition (torus), represented by $ \LL = (\Z/X\cdot \Z) \times (\Z/Y\cdot \Z)$, where  $X$ and $Y$ are the dimensions of the lattice. 
In this text, we only consider square lattices, that is, we take $ X = Y = L$, where $ L $ is called the {\em size} of the lattice.

The set of states is that each cell can hold is itself composed of two sets: the first part is a ternary state and the second part corresponds to the data. 
The set of states is thus a Cartesian product $Q \times \DD$. 
The first set is defined as $Q = \{\NN, \EE, \RR\}$, where the states $ \NN$, $\EE$, $\RR$, respectively correspond to the {\em excited}, {\em neutral} and {\em refractory} states. 
The second set $ \DD $ is dependent on the data set which is considered. We will assume that the set $ D $ contains a particular value, called the {\em void} value, denoted by $ V $, which represents the absence of data in a given cell.

Formally, for a cell $c\in\LL$ and time $ t \in \N$, we denote by $\sigma^{t}_{c} \in Q $ the state of this cell and by $\rho^{t}_{c} \in \DD $ its data.
For the sake of brevity, we will say {\em state} only to refer to the first part of the cell and say {\em data} for the second part. The reader should note that the data of a cell typically do not correspond to the data of its amoeba. It corresponds to the data propagated by the environment.

The set $\Am{t}{c}$ represents the amoebae contained in a cell $ c $. Formally, $ \Am{t}{c} $ is represented by a {\em multi-set} of elements of $ D $, that is, it may contain several elements of $ D $ with an identical value. Recall that each amoeba is in charge of carrying an element of the dataset that has to be processed. 

The interactions between cells follow a local rule and we use here the Moore neighbourhood, that is, a cell $ c $ will only ``see'' its own state and the state of its 8 nearest neighbours. This set is denoted by $ \Neighb(c)$.

In all the experiments that follow, the initial condition of the system is that all cells are initially in the neutral state $\NN$ and the elements of the dataset are randomly and uniformly distributed on the grid. For the data, we simply assign to each cell a given probability $\dini$ to contain a data, which is drawn at random according to a non-uniform distribution that will be described later. The probability $\dini$ represents intuitively the initial \emph{density} of data, hence the use of the letter d.

Informally, our model relies on three main features: (a) the amoebae have the ability to randomly trigger reaction-diffusion waves that will make other amoebae react; (b) these waves propagate without attenuation and by partially colliding and merging when they meet ; (c) when amoebae sees such a wave in its environment, it reacts by being attracted or repulsed according to the data it sees and its own data.
Let us now describe these steps more precisely.


\subsection{The reaction-diffusion waves}

\figExcitationWaves

The rules that govern the propagation of reaction-diffusion waves are simple: (a) A neutral cell becomes excited if it has an excited neighbour; (b) an excited cell always becomes refractory at the next time step: (c) a refractory cell always become neutral at the next time step. 
Once a neutral cell gets excited, the excitations propagate in form of waves formed by lines of excited cells that we call {\em fronts}, which are followed lines of refractory cells (Fig. \ref{fig:Confrontation_excitation}). The role of the refractory state is to prevent the retropropagation of excitations. 
When two waves meet, they annihilate if the fronts propagate in opposite directions and merge if the fronts propagate in the same direction.


In addition to the possibility to being excited by its neighbours, a cell which contains at least one amoeba becomes excited with a probability $\lambda$, called the \emph{excitation rate}.
In this case, the excited cell takes the state $ \EE $, and its data $\rho^{t}_{c}$ will be chosen randomly and uniformly among the data of
amoebae it contains.
This choice is represented by $ R[\Am{t}{c} ] $ where the operation of random uniform choice in a multiset $ X $ is denoted by $ R[X] $. We note $B(p)$ as a Bernoulli law of probability $p$.

We denote by $ E^{t}_{c}= \{ c' \in N(c): \sigma^{t}_{c} = \EE \}$ the set of cells which are in the excited state in the neighbourhood of $c$ and by $ D^t_c = \{ \rho^{t}_{c}: c \in E^{t}_{c} \}$ the set of data that these cells contain. 
It should be noted that in general the values of the data contained in a given wave are uniform, as they come from the same amoeba source. However, when two waves of excitation collide and merge, a given cell might see in its neighbour two excited cells with different data, in which case it will choose randomly and uniformly in the multiset of values it sees.
%In addition, the amoeba contains one of the data to be sorted: this data will be transmitted in the excitation signal, allowing an interaction between this signal and the other amoeba.


%When a cell $c$ at time $t$ is excited, it has a rated value $\rho^{t}_{c} \in D$. 


The status of a cell is updated as follows: \\

If $\sigma^{t}_{c} =$ \NN
\begin{equation*}
    \text{if } \Am t c \ne \emptyset \text{ and } B(\lambda) = 1 \text{ then } \sigma^{t+1}_{c} = \EE \text { and } \rho^{t+1}_{c} = R[\Am t c ] \\
\end{equation*} 
\begin{equation*}
    \text{else if } card(E^{t}_{c}) > 0 \hspace*{1cm}\hspace*{0.05cm} \text{ then } \sigma^{t+1}_{c} = \EE \text{ and } \rho^{t+1}_{c} = R[D^{t}_{c}] \\
\end{equation*}

else if $\sigma^{t}_{c} =$ \EE \hspace*{4.3cm} then $\sigma^{t+1}_{c} =$ \RR \text{ and } $\rho^{t+1}_{c} = V$ \\

else if $\sigma^{t}_{c} =$ \RR \hspace*{4.3cm} then $\sigma^{t+1}_{c} =$ \NN \text{ and } $\rho^{t+1}_{c} = V$ \\

else $\sigma^{t+1}_{c} =$ \NN \text{ and } $\rho^{t+1}_{c} = V$.\\

We find our different cases: the self-excitation of a cell by an amoeba, the excitation by a neighbouring cell, the de-excitement and the return to the neutral state.

\subsection{Movements of the amoebae}

Let us describe the movements of the amoebae, which take place in the second layer of the model. To end, we introduce some intermediary definitions.
A cell is \emph{empty} if does not contain any amoeba and it is \emph{available} it is either empty of it contains only one amoeba. 
In order to limit the number of amoebae (that is data) contained per cell we use the following rules:
(a) Amoebae can move only to the available cells of their neighbourhood.
(b) If a cell contains several amoebae, only one may move to a neighbour.
(c) There is no limit on how many amoebae an available cell can simultaneously receive.

Let us now describe how the amoebae react to the presence of excited cells in their neighbourhood. 
The key point of the model is that an excitation can be either attractive or repulsive for an amoeba, depending on the value of the excitation and the data of the amoeba.
The use of a repulsive interaction is a novelty compared to the previous models using amoebae (see Introduction) and its purpose is to separate the groups of amoebae that will correspond to different data clusters.
%however it leads to getting sets of sorted but glued sets. 

%We will define a rule to determine whether the response of a $c$ cell amoeba to a neighbouring excitation is attractive or repulsive because amoeba have no type. 
The idea is that for each excited cell in the neighbourhood, we will determine the excitation is attractive cell or repulsive according to a probability law, denoted by $ \patt $, depending on the absolute value of the difference $ \delta $ between the value of the amoeba and the value of the nearby excitation.

The probability  $ \patt $ to be attractive is defined with the following equation: $\patt(\delta) = e^{-\delta/\Lambda}$, that is, the higher $ \delta $ is, the lower the probability to be attractive and thus the higher the probability to be repulsed. The constant $ K $ is set by the user of the model and depends on the dataset that is used; it roughly corresponds to the expected distance between the clusters of the dataset.

%The value of $K$ is chosen to be as suitable as possible for the datasets used. The advantage of the probability rule is not to perform binary behaviour, so there will always be an attractive probability according to the $\delta$. Now that $P_{att}$ is set, we can for a given amoeba of a $c$ cell express the local amoeba rules.

Naturally, a problem arises when a cell contains several amoebae. In this case, we simply choose uniformly at random which of the amoebae is selected to move and we calculate $ \delta $ according the data of the selected amoeba. 

For a given cell $ c \in L$ which contains one amoeba or more, we define for each cell $ c' $ in the neighbourhood of $ c$ the quantity $\delta_{c,c'}=\left\lvert \rho^{t}_{c'}-R[\Am t c ] \right\rvert$. (Note that the random value $R[\Am t c ]$ is chosen once and not for each neighbour.) 
We map $\delta_{c,c'}$ to a value $ \dstar_{c,c'} \in \{\texttt{A},\texttt{R}\} $, where \texttt{A} and \texttt{R} respectively denote an attractive or repulsive signal.

To express the local rules which define the movements of the amoebae, we need to define three sets. 
For a given cell $ c $ the set $ \Nset $ is the set of available neighbours: $ \Nset = \{ c' \in \Neighb(c): \card( \Am t {c'} ) \in \set{0,1} \}$. 
The sets $ \Aset $ (resp. $ \Rset $) is the set of available neighbours that are {attractive} (resp. repulsive): 
$ \Aset = \{ c' \in \Nset: \dstar_{c,c'}=\texttt{A} \}$ and 
$ \Rset = \{ c' \in \Nset: \dstar_{c,c'}=\texttt{R} \}$.
The set $ \Eset $ is the complement of the previous set; it is the set of cells where an amoeba can {\em escape} in case of repulsion: $ \Eset =\Nset \setminus \Rset$. All these sets are represented in fig. \ref{fig:RepresentationSets}.

\figRepresentationSets

The movements will determined by a list of rules with an imposed order of priority: an amoeba may make a random movement to an available cell according to the probability $\Pag$, the phenomenon of attraction and finally that of repulsion. Second, the priority is given to attraction over repulsion, that is, if an amoeba sees at least one excited cell $\texttt{A}$, it chooses attraction, otherwise it will choose a repulsive movement.

Denoting by $ \Delta^{t}_{c} $ the cell where the selected amoeba moves, this yields:
$$ \Delta^{t}_{c} =
\begin{cases}
R[\Nset] & \text{ if } B(P_{\mathrm{a}}) = 1 \text{ and } \card(\Nset) > 0\\
R[\Aset] & \text{ if } \sigma^{t}_{c}=\NN \text{ and } \card(\Aset) > 0\\
R[\Eset] & \text{ if } \sigma^{t}_{c}=\NN \text{ and } \card(\Rset) > 0 \text{ and } \card(\Eset) > 0\\
c &  \text{ otherwise. }
\end{cases}
$$
 
These rules represent the four possible movements: by agitation, by attraction, by repulsion and finally staying in the same cell. The positions of the amoebae are then updated synchronously for each non-empty cell $ c $ with the movement from $ c $ to $ \Delta^{t}_{c} $.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Study of the model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\subsection{A first observation of the behaviour}
\label{sec:qualExp}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\figOneSimulationInFourPictures

Let us now observe some simulations. Figure~\ref{fig:Representation_detaille} shows the clustering process on a lattice of size $ L = 50 $ cells. The initial condition is set with $ \dini = 0.1 $, that is, each cell has a probability of 10\% to contain a data.
%
The value initially attributed to the amoebae is an integer drawn randomly in the interval $I =[\![0,\;400]\!]$. The data is not uniformly distributed: its distribution is the sum of two Gaussian laws of parameters $(\mu_{1},\sigma_1)$ and $(\mu_{2},\sigma_2)$ where the mean values are set to $\mu_{1}=100$ and $\mu_{2}=300$ and the standard deviations are set to $\sigma_1 = \sigma_2 = 0.2$. 

The parameters settings were determined empirically after a series of tests. We set the value of the emission rate of waves to $\lambda = 0.01$ and the attraction probability law $ \patt $ is set with $\Lambda=40$. In order to introduce some ``difficulty'' in the clustering process, we also add some agitation, setting $\Pag = 0.1$, that is, the amoebae will make a random move every ten steps in average.

One can observe the following general behaviour: the amoebae start forming small groups where the type is rather homogeneous%(Fig. \ref{fig:Representation_detaille}.2)
. Thanks to the randomness of the excitation waves, these groups progressively merge until they form larger groups. 

At this stage two possibilities are generally observed: either the large groups directly form two well-sorted clusters, or the sorting stagnates with approximately two or three formed groups and a few isolated amoebae.
% (Fig. \ref{fig:Representation_detaille}.3). 
In this case, the sorting process is somehow delayed due to two main reasons: (a) isolated amoebae may find themselves blocked between the different excitations, and (b) two groups of amoebae that contain amoebae of the same type might try to get closer but get repelled by another large group formed of amoebae of the opposite type. However, as one can observe on Fig.~\ref{fig:Representation_detaille}, the clusters are well formed after a few thousand steps and remain stable over time.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Quantify sorting progress and efficiency}
\label{sec:criteria}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We now describe how to evaluate more quantitatively the evolution of the clustering process. 
We continue to consider the simple case where the system should separate the data into two clusters only.
Our problem is to evaluate, for a given distribution of amoebae on the grid, how far we are from our goal. Our clustering estimator will associate to each data a type, A or B, depending on its value. For example if the data is distributed in the range of integers from 0 to 100, {\em type-A} data will cover the range 0 to 49 and {\em type-B} data will cover the rest of the interval. 

With this particular setting of the problem, two main features can be isolated: (a) {\em homogeneity}: once the groups of connected amoebae are identified, we estimate to which extent they are mixed or not. (b)
{\em compactness}: we estimate how far the global configuration of amoebae is from forming two main groups.

For a given configuration, we thus partition the set of amoebae into connected components of amoebae, where Moore's neighbourhood is used to define the connexity property. Let us denote by $ C_1, \dots, C_k $ the components obtained, with the convention that $ C_1 $ and $ C_2$ are respectively the largest and second largest sets (in terms of number of amoebae they contain). Given two components $ i $ and $ j $, we denote by $ \dH(i,j) $ the Hausdorff distance between these two components. This distance is frequently used to evaluate distances between sets of points and displays various good analytical properties~\cite{article_distance_Hausdorff}. We then make a partition of the set components $ \{ C_1, ..., C_k \} $ into two sets of components $ P_1 $ and $ P_2 $ by assigning each component $ C_i $ to $ P_1 $ if the distance $\dH(C_i,C_1) $ is smaller than $ \dH(C_i,C_2) $ and to $ P_2 $ otherwise.

To define the \emph{homogeneity} criterion, we simply calculate the number of amoebae $ n_1^A $ and $ n_1^B $ (resp.  $ n_2^A $ and $ n_2^B $) that are of respective type A and B that are contained in $ P_1 $ (resp. in $ P_2 $). 
The \emph{homogeneity} of $ P_m $ for  $ m \in \{1,2\} $ is then set equal to $ h_m= \left\lvert n_m^A-n_m^B \right\rvert / (n_m^A+~n_m^B) $. The global homogeneity $ h $ is then simply the minimum of $ h_1 $ and $ h_2$.
This form is chosen such that a homogeneous set of components will lead to $ h =1 $ while a set of components with an equal number of amoebae of each type will give us $ h=0 $.

Let us now examine the {\em compactness criterion}. 
We first calculate the average distance between the components and the main component they belong to; that is,
\[ \tilde{d} = \frac{1}{k} \cdot \big( \Sum{C_i \in P_1}{} \dH (C_i,C_1) +  \Sum{C_i \in P_2}{} \dH (C_i,C_2) \,\,\big) \]
%+ \Sum_{C_i \in P_2} \dH(C_i,C2) )$$.
We compare this value to the distance between the two main components of the system $\dH(C_1,C_2)$. 
The compactness $ \kappa $ is then defined as $ \kappa = 1- \tilde{d}/ \dH(C_1,C_2) $; a form that is chosen in order to approach 1 as $ \tilde{d} $ gets smaller (while $\dH(C_1,C_2) $ remains stable).

%Note that the compactness criterion is negative when the distances between the secondary groups and their closest main group are greater than the distance between the two main groups. [For the sake of having a better graphical representation, we choose to set the value to zero when it is negative.]

The combination of these two criteria will give us an estimation of how the clustering process evolves in time. Note that criteria are not defined when we have only one component and note that we considered here the case of two types, but this method can of course be extended to any number of types. It should also be emphasised that the types are only given for the analysis of the process, but that the process itself never uses this information in the local rules it applies. Let us now observe the clustering process on some simple examples.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Quantitative analysis of the behaviour}
\label{sec:quantExp}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\figExempleSimulationAndCriteria

\figGraphCriteria

Let us now analyse the relevance of the two criteria introduced above to quantify the progress of the sorting of amoebae in the system. 
The two connected components of amoebae are illustrated Fig.~\ref{fig:Exemple_simulation} and the evolution of the temporal values of these criteria are represented on Fig.~\ref{fig:criteriaTime}. The parameters are set identically to those exposed in Sec.~\ref{sec:qualExp}.

%The first criterion, is a criterion on the homogeneity of the formed assemblies. The greater the number of same amoebae in a set, the greater the homogeneity criterion of the model. 
Let us first put our attention on the {\em homogeneity} criterion.
One can observe that during the first moments of the simulation, the amoebae are scattered in space and the two main sets are very heterogeneous, which leads to a low homogeneity. 
This continues until amoebae form larger groups of amoebae with similar data, which yields a gradual increase in the homogeneity criterion, with an additional noise due to the randomness inherent to the model. 
As mentioned earlier, after this stage is complete, two possibilities generally occur: we either have a rather steady increase of the criteria, which indicates that the amoebae are sorted into two groups, or the values will stagnate for a while (as shown on Fig.~\ref{fig:Exemple_simulation}), and then find the sorted stable. 

The stochastic nature of the model makes that some evolutions may last longer to reach the sorted state.
For instance, when three main groups of amoebae are formed, with one group of one type and the other two of the second type, the value of homogeneity remains around 50\% for a moment until the two groups of the same type can merge. 
%The other way is when two groups, one formed and the second trying to get together, cause a variation around the 50\%. After these transitional steps, amoebae are sorted into only two groups when the criterion is 100\%. However, we observe that sorting reaches a stability well before, around 80\%, where two groups are formed with isolated amoebae moving towards them. \\

The {\em compactness} criterion allows us to complete our analysis of the clustering process. 
Recall it estimates the average relative distance of the connected components of the amoebae to the closet main component. In the beginning of the simulation, since amoebae are scattered randomly in space and these main components are not stable and the criterion is of little relevance. 
As one can observe, the compactness criterion becomes relevant only when larger groups of amoebae are formed, in which case its values become greater than 25\%. 

Note that when the clustering has occurred (that is, when we have only two groups), the homogeneity approaches 100\% but the compactness stabilises at lower values (between 40 and 50\%).
Indeed, the rules we imposed on the movements of amoebae (agitation, limit of their number per cell) do not allow them to form perfect components. 
%This criterion of compactness is therefore sensitive but makes it possible to observe the distribution of amoeba in space during group gatherings. \\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Robustness of the model}
\label{sec:robur}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\figConvTimeRegularObstacles

\paragraph{Data.}

\figThreeDistrib

Let us examine how the model responds to variations of the distribution of data. 
We take again the interval $I =[\![0,\;400]\!]$ and generate the data distribution according to the sum of two Gaussian laws of parameters $(\mu_{1},\sigma_1)$ and $(\mu_{2},\sigma_2)$. 
We fix the value of $ \Lambda $ to $\Lambda=40$ and the standard deviation to $ \sigma_1= \sigma_2 = 0.4 $.

We perform different experiments by varying $ (\mu_{1},\mu_{2})$. 
We define the {\em convergence time} as the number of time steps that are needed to attain both a homogeneity greater than $h^*=0.8$ and compactness of $ \kappa^*=0.25$. These values are fixed empirically by observing the situations which correspond to a good clustering quality and a relatively stable global state. Since we aim at achieving a rapid convergence, we stop the simulation after 3000 time steps.

The results are presented on Fig.~\ref{fig:threeDistrib}. 
We observe that the clustering is rapid when the two Gaussian laws are well separated $(\mu_{1},\mu_{2})=(100,300)$. 
More surprising, we note that when the averages are made closer $(\mu_{1},\mu_{2})=(125,275)$, and even though the two laws begin to overlap, the convergence time remains low and is even better than the previous case. 
A limit case is observed for $(\mu_{1},\mu_{2})=(150,250)$ where the convergence time jumps above the limit value of 3000 steps. A visual inspection of the simulations and an observation of the evolution of the criteria shows that on the two first case, the clusters remain stable and well-sorted after the convergence time. Of course, $ \Lambda $ values are more suitable for some datasets but we here fix $ \Lambda $ for the sake of brevity.

%\figTripleGaussian

We also examined the behaviour of the model when the data is a superposition of three Gaussian laws: in this case,
% as observed on Fig.~\ref{fig:Trois_gaussiennes}, 
we observe that the clustering process occurs as expected by sorting the data into three groups, without any significant delay when $ \lambda $ is set to the appropriate value.


\paragraph{Initial conditions.}
One may also examine what happens if a large number of amoebae is initially introduced. 
A new series of simulations is carried out with the following initial condition for an initial density between 10\% and 75\%: a cell has a chance of initially containing an amoeba containing a data. 
We empirically observed that although the movements of the amoebae are more constrained and although the propagation of cell excitations are more difficult, the model succeeds in achieving the clustering operation in times that are comparable to those expressed above. 
It is only when the initial density reaches $ \dini = 0.75 $ that strong difficulties appear and that the convergence time significantly rises. This suggest that the size of the lattice should always be set in agreement with the quantity of data that needs to be processed; with say at least three times as many cells as amoebae. 

\paragraph{Obstacles.}
As mentioned earlier, one of the motivations for the use of bio-inspired models is their robustness to errors and failures in the computing medium. 
We thus consider the case where obstacles can be present on the lattice, that is, cells that cannot transmit any information which cannot hold any amoeba. 

Simulations in such conditions are represented on {Fig.~\ref{fig:Etude_robustesse}: we consider different values of the emission rate $\lambda$ with and without obstacles. 
One can observe that the overall behaviour is not perturbed by the presence of obstacles, although some isolated amoebae may take a longer time to join other groups. The convergence of the amoebae can be slowed down but we empirically observed that the process always manages to achieve the clustering task. 

%\smallskip
%\paragraph{Openings.}
\section{Openings}
These first tests are rather encouraging and show that our model remains robust to various modifications of its settings. The particularity of our bio-inspired method is its specific use of {\em randomness}: the moments where the amoebae emit their waves is determined randomly, and should be set in order to allow a good balance between ``speaking'' and ``hearing''. The clustering task is also realised by achieving an appropriate setting to determine whether the data seen in the excitation waves are attractive or repulsive. We here only presented a prototype tested on a rather simple clustering task with only two groups to form. The model thus now needs to be evaluated on more realistic datasets and its robustness should be examined thoroughly in more stringent conditions. Our first informal observations on other conditions are rather encouraging and such explorations could be performed by all researchers who interested in the exploration of the field of bio-inspired cellular models.
